# Plateforme des Correspondances des Jeunes Mathématicien·nes

[![pipeline status](https://gitlab.com/animath/si/plateforme-corres2math/badges/master/pipeline.svg)](https://gitlab.com/animath/si/plateforme-corres2math/-/commits/master)
[![coverage report](https://gitlab.com/animath/si/plateforme-corres2math/badges/master/coverage.svg)](https://gitlab.com/animath/si/plateforme-corres2math/-/commits/master)

La plateforme des Correspondances des Jeunes Mathématicien·nes est née pour la seconde édition en 2019 de l'action.
D'abord codée en PHP, elle a subi une refonte totale en Python, à l'aide du framework Web [Django](https://www.djangoproject.com/).

Cette plateforme permet aux participants et encadrants de s'inscrire et de déposer leurs autorisations nécessaires.
Ils pourront ensuite déposer leurs solutions et notes de synthèse pour le premier tour en temps voulu. La plateforme 
offre également un accès pour les organisateurs et les jurys leur permettant de communiquer avec les équipes et de
récupérer les documents nécessaires.

Un wiki plus détaillé arrivera ultérieurement. L'interface organisateur et jury est vouée à être plus poussée.

L'instance de production est disponible à l'adresse [inscription.correspondances-maths.fr](https://inscription.correspondances-maths.fr).

## Installation

Le plus simple pour installer la plateforme est d'utiliser l'image Docker incluse, qui fait tourner un serveur Nginx
exposé sur le port 80 avec le serveur Django. Ci-dessous une configuration Docker-Compose, à adapter selon vos besoins :

```yaml
  plateforme-corres2math:
    build: https://gitlab.com/animath/si/plateforme-corres2math.git
    links:
      - postgres
    ports:
      - "80:80"
    env_file:
      - ./inscription-corres2math.env
    volumes:
    # - ./inscription-corres2math:/code
      - ./inscription-corres2math/media:/code/media
```

Le volume `/code` n'est à ajouter uniquement en développement, et jamais en production.

Il faut remplir les variables d'environnement suivantes :

```env
CORRES2MATH_STAGE=              # dev ou prod
DJANGO_DB_TYPE=                 # MySQL, PostgreSQL ou SQLite (par défaut)
DJANGO_DB_HOST=                 # Hôte de la base de données
DJANGO_DB_NAME=                 # Nom de la base de données
DJANGO_DB_USER=                 # Utilisateur de la base de données
DJANGO_DB_PASSWORD=             # Mot de passe pour accéder à la base de données
SMTP_HOST=                      # Hôte SMTP pour l'envoi de mails
SMTP_PORT=465                   # Port du serveur SMTP
SMTP_HOST_USER=                 # Utilisateur du compte SMTP
SMTP_HOST_PASSWORD=             # Mot de passe du compte SMTP
FROM_EMAIL=contact@correspondances-maths.fr     # Nom de l'expéditeur des mails
SERVER_EMAIL=contact@correspondances-maths.fr   # Adresse e-mail expéditrice
SYMPA_URL=lists.example.com     # Serveur Sympa à utiliser
SYMPA_EMAIL=                    # Adresse e-mail du compte administrateur de Sympa
SYMPA_PASSWORD=                 # Mot de passe du compte administrateur de Sympa
SYNAPSE_PASSWORD=               # Mot de passe du robot Matrix 
```

Si le type de base de données sélectionné est SQLite, la variable `DJANGO_DB_HOST` sera utilisée en guise de chemin vers
le fichier de base de données (par défaut, `db.sqlite3`).

En développement, il est recommandé d'utiliser SQLite pour des raisons de simplicité. Les paramètres de mail ne seront
pas utilisés, et les mails qui doivent être envoyés seront envoyés dans la console. Les intégrations mail et Matrix
seront également désactivées.

En production, il est recommandé de ne pas utiliser SQLite pour des raisons de performances.

La dernière différence entre le développment et la production est qu'en développement, chaque modification d'un fichier
est détectée et le serveur se relance automatiquement dès lors.