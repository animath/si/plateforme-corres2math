# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.forms import FileInput
from django.utils.translation import gettext_lazy as _

from .models import AdminRegistration, CoachRegistration, StudentRegistration


class SignupForm(UserCreationForm):
    """
    Signup form to registers participants and coaches
    They can choose the role at the registration.
    """

    role = forms.ChoiceField(
        label=lambda: _("role").capitalize(),
        choices=lambda: [
            ("participant", _("participant").capitalize()),
            ("coach", _("coach").capitalize()),
        ],
    )

    def clean_email(self):
        """
        Ensure that the email address is unique.
        """
        email = self.data["email"]
        if User.objects.filter(email=email).exists():
            self.add_error("email", _("This email address is already used."))
        return email

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["first_name"].required = True
        self.fields["last_name"].required = True
        self.fields["email"].required = True

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'password1', 'password2', 'role',)


class UserForm(forms.ModelForm):
    """
    Replace the default user form to require the first name, last name and the email.
    The username is always equal to the email.
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["first_name"].required = True
        self.fields["last_name"].required = True
        self.fields["email"].required = True

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email',)


class StudentRegistrationForm(forms.ModelForm):
    """
    A student can update its class, its school and if it allows Animath to contact him/her later.
    """
    class Meta:
        model = StudentRegistration
        fields = ('team', 'student_class', 'school', 'give_contact_to_animath', 'email_confirmed',)


class PhotoAuthorizationForm(forms.ModelForm):
    """
    Form to send a photo authorization.
    """
    def clean_photo_authorization(self):
        if "photo_authorization" in self.files:
            file = self.files["photo_authorization"]
            if file.size > 2e6:
                raise ValidationError(_("The uploaded file size must be under 2 Mo."))
            if file.content_type not in ["application/pdf", "image/png", "image/jpeg"]:
                raise ValidationError(_("The uploaded file must be a PDF, PNG of JPEG file."))
            return self.cleaned_data["photo_authorization"]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields["photo_authorization"].widget = FileInput()

    class Meta:
        model = StudentRegistration
        fields = ('photo_authorization',)


class CoachRegistrationForm(forms.ModelForm):
    """
    A coach can tell its professional activity.
    """
    class Meta:
        model = CoachRegistration
        fields = ('team', 'professional_activity', 'give_contact_to_animath', 'email_confirmed',)


class AdminRegistrationForm(forms.ModelForm):
    """
    Admins can tell everything they want.
    """
    class Meta:
        model = AdminRegistration
        fields = ('role', 'give_contact_to_animath', 'email_confirmed',)
