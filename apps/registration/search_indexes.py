# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from haystack import indexes

from .models import Registration


class RegistrationIndex(indexes.ModelSearchIndex, indexes.Indexable):
    """
    Registrations are indexed by the user detail.
    """
    text = indexes.NgramField(document=True, use_template=True)

    class Meta:
        model = Registration
