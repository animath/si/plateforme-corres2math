# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.urls import path

from .views import MyAccountDetailView, ResetAdminView, SignupView, UserDetailView, UserImpersonateView, \
    UserListView, UserResendValidationEmailView, UserUpdateView, UserUploadPhotoAuthorizationView, UserValidateView, \
    UserValidationEmailSentView

app_name = "registration"

urlpatterns = [
    path("signup/", SignupView.as_view(), name="signup"),
    path('validate_email/sent/', UserValidationEmailSentView.as_view(), name='email_validation_sent'),
    path('validate_email/resend/<int:pk>/', UserResendValidationEmailView.as_view(),
         name='email_validation_resend'),
    path('validate_email/<uidb64>/<token>/', UserValidateView.as_view(), name='email_validation'),
    path("user/", MyAccountDetailView.as_view(), name="my_account_detail"),
    path("user/<int:pk>/", UserDetailView.as_view(), name="user_detail"),
    path("user/<int:pk>/update/", UserUpdateView.as_view(), name="update_user"),
    path("user/<int:pk>/upload-photo-authorization/", UserUploadPhotoAuthorizationView.as_view(),
         name="upload_user_photo_authorization"),
    path("user/<int:pk>/impersonate/", UserImpersonateView.as_view(), name="user_impersonate"),
    path("user/list/", UserListView.as_view(), name="user_list"),
    path("reset-admin/", ResetAdminView.as_view(), name="reset_admin"),
]
