# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from corres2math.tokens import email_validation_token
from django.contrib.sites.models import Site
from django.db import models
from django.template import loader
from django.urls import reverse_lazy
from django.utils.crypto import get_random_string
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.translation import gettext_lazy as _
from polymorphic.models import PolymorphicModel


class Registration(PolymorphicModel):
    """
    Registrations store extra content that are not asked in the User Model.
    This is specific to the role of the user, see StudentRegistration,
    ClassRegistration or AdminRegistration..
    """
    user = models.OneToOneField(
        "auth.User",
        on_delete=models.CASCADE,
        verbose_name=_("user"),
    )

    give_contact_to_animath = models.BooleanField(
        default=False,
        verbose_name=_("Grant Animath to contact me in the future about other actions"),
    )

    email_confirmed = models.BooleanField(
        default=False,
        verbose_name=_("email confirmed"),
    )

    def send_email_validation_link(self):
        """
        The account got created or the email got changed.
        Send an email that contains a link to validate the address.
        """
        subject = "[Corres2math] " + str(_("Activate your Correspondances account"))
        token = email_validation_token.make_token(self.user)
        uid = urlsafe_base64_encode(force_bytes(self.user.pk))
        site = Site.objects.first()
        message = loader.render_to_string('registration/mails/email_validation_email.txt',
                                          {
                                              'user': self.user,
                                              'domain': site.domain,
                                              'token': token,
                                              'uid': uid,
                                          })
        html = loader.render_to_string('registration/mails/email_validation_email.html',
                                       {
                                           'user': self.user,
                                           'domain': site.domain,
                                           'token': token,
                                           'uid': uid,
                                       })
        self.user.email_user(subject, message, html_message=html)

    @property
    def type(self):  # pragma: no cover
        raise NotImplementedError

    @property
    def form_class(self):  # pragma: no cover
        raise NotImplementedError

    @property
    def participates(self):
        return isinstance(self, StudentRegistration) or isinstance(self, CoachRegistration)

    @property
    def is_admin(self):
        return isinstance(self, AdminRegistration) or self.user.is_superuser

    @property
    def matrix_username(self):
        return f"corres2math_{self.user.pk}"

    def get_absolute_url(self):
        return reverse_lazy("registration:user_detail", args=(self.user_id,))

    def __str__(self):
        return f"{self.user.first_name} {self.user.last_name}"

    class Meta:
        verbose_name = _("registration")
        verbose_name_plural = _("registrations")


def get_random_filename(instance, filename):
    return "authorization/photo/" + get_random_string(64)


class StudentRegistration(Registration):
    """
    Specific registration for students.
    They have a team, a student class and a school.
    """
    team = models.ForeignKey(
        "participation.Team",
        related_name="students",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
        default=None,
        verbose_name=_("team"),
    )

    student_class = models.IntegerField(
        choices=[
            (12, _("12th grade")),
            (11, _("11th grade")),
            (10, _("10th grade or lower")),
        ],
        verbose_name=_("student class"),
    )

    school = models.CharField(
        max_length=255,
        verbose_name=_("school"),
    )

    photo_authorization = models.FileField(
        verbose_name=_("photo authorization"),
        upload_to=get_random_filename,
        blank=True,
        default="",
    )

    @property
    def type(self):
        return _("student")

    @property
    def form_class(self):
        from registration.forms import StudentRegistrationForm
        return StudentRegistrationForm

    class Meta:
        verbose_name = _("student registration")
        verbose_name_plural = _("student registrations")


class CoachRegistration(Registration):
    """
    Specific registration for coaches.
    They have a team and a professional activity.
    """
    team = models.ForeignKey(
        "participation.Team",
        related_name="coachs",
        on_delete=models.PROTECT,
        null=True,
        blank=True,
        default=None,
        verbose_name=_("team"),
    )

    professional_activity = models.TextField(
        verbose_name=_("professional activity"),
    )

    @property
    def type(self):
        return _("coach")

    @property
    def form_class(self):
        from registration.forms import CoachRegistrationForm
        return CoachRegistrationForm

    class Meta:
        verbose_name = _("coach registration")
        verbose_name_plural = _("coach registrations")


class AdminRegistration(Registration):
    """
    Specific registration for admins.
    They have a field to justify they status.
    """
    role = models.TextField(
        verbose_name=_("role of the administrator"),
    )

    @property
    def type(self):
        return _("admin")

    @property
    def form_class(self):
        from registration.forms import AdminRegistrationForm
        return AdminRegistrationForm

    class Meta:
        verbose_name = _("admin registration")
        verbose_name_plural = _("admin registrations")
