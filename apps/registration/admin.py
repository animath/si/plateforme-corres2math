# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.contrib import admin
from polymorphic.admin import PolymorphicChildModelAdmin, PolymorphicParentModelAdmin

from .models import AdminRegistration, CoachRegistration, Registration, StudentRegistration


@admin.register(Registration)
class RegistrationAdmin(PolymorphicParentModelAdmin):
    child_models = (StudentRegistration, CoachRegistration, AdminRegistration,)
    list_display = ("user", "type", "email_confirmed",)
    polymorphic_list = True


@admin.register(StudentRegistration)
class StudentRegistrationAdmin(PolymorphicChildModelAdmin):
    pass


@admin.register(CoachRegistration)
class CoachRegistrationAdmin(PolymorphicChildModelAdmin):
    pass


@admin.register(AdminRegistration)
class AdminRegistrationAdmin(PolymorphicChildModelAdmin):
    pass
