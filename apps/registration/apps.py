# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.apps import AppConfig
from django.db.models.signals import post_save, pre_save


class RegistrationConfig(AppConfig):
    """
    Registration app contains the detail about users only.
    """
    name = 'registration'

    def ready(self):
        from registration.signals import create_admin_registration, invite_to_public_rooms, \
            set_username, send_email_link
        pre_save.connect(set_username, "auth.User")
        pre_save.connect(send_email_link, "auth.User")
        post_save.connect(create_admin_registration, "auth.User")
        post_save.connect(invite_to_public_rooms, "registration.Registration")
        post_save.connect(invite_to_public_rooms, "registration.StudentRegistration")
        post_save.connect(invite_to_public_rooms, "registration.CoachRegistration")
        post_save.connect(invite_to_public_rooms, "registration.AdminRegistration")
