# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from corres2math.lists import get_sympa_client
from corres2math.matrix import Matrix
from django.contrib.auth.models import User

from .models import AdminRegistration, Registration


def set_username(instance, **_):
    """
    Ensure that the user username is always equal to the user email address.
    """
    instance.username = instance.email


def send_email_link(instance, **_):
    """
    If the email address got changed, send a new validation link
     and update the registration status in the team mailing list.
    """
    if instance.pk:
        old_instance = User.objects.get(pk=instance.pk)
        if old_instance.email != instance.email:
            registration = Registration.objects.get(user=instance)
            registration.email_confirmed = False
            registration.save()
            registration.user = instance
            registration.send_email_validation_link()

            if registration.participates and registration.team:
                get_sympa_client().unsubscribe(old_instance.email, f"equipe-{registration.team.trigram.lower()}", False)
                get_sympa_client().subscribe(instance.email, f"equipe-{registration.team.trigram.lower()}", False,
                                             f"{instance.first_name} {instance.last_name}")


def create_admin_registration(instance, **_):
    """
    When a super user got created through console,
    ensure that an admin registration is created.
    """
    if instance.is_superuser:
        AdminRegistration.objects.get_or_create(user=instance)


def invite_to_public_rooms(instance: Registration, created: bool, **_):
    """
    When a user got registered, automatically invite the Matrix user into public rooms.
    """
    if created:
        Matrix.invite("#annonces:correspondances-maths.fr", f"@{instance.matrix_username}:correspondances-maths.fr")
        Matrix.invite("#faq:correspondances-maths.fr", f"@{instance.matrix_username}:correspondances-maths.fr")
        Matrix.invite("#je-cherche-une-equipe:correspondances-maths.fr",
                      f"@{instance.matrix_username}:correspondances-maths.fr")
        Matrix.invite("#flood:correspondances-maths.fr", f"@{instance.matrix_username}:correspondances-maths.fr")
