# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django import template
from django_tables2 import Table
from participation.models import Participation, Team, Video
from participation.tables import ParticipationTable, TeamTable, VideoTable

from ..models import Registration
from ..tables import RegistrationTable


def search_table(results):
    model_class = results[0].object.__class__
    table_class = Table
    if issubclass(model_class, Registration):
        table_class = RegistrationTable
    elif issubclass(model_class, Team):
        table_class = TeamTable
    elif issubclass(model_class, Participation):
        table_class = ParticipationTable
    elif issubclass(model_class, Video):
        table_class = VideoTable
    return table_class([result.object for result in results], prefix=model_class._meta.model_name)


register = template.Library()
register.filter("search_table", search_table)
