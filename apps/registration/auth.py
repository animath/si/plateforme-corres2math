# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from cas_server.auth import DjangoAuthUser  # pragma: no cover


class CustomAuthUser(DjangoAuthUser):  # pragma: no cover
    """
    Override Django Auth User model to define a custom Matrix username.
    """

    def attributs(self):
        d = super().attributs()
        if self.user:
            d["matrix_username"] = self.user.registration.matrix_username
            d["display_name"] = str(self.user.registration)
        return d
