# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.utils.translation import gettext_lazy as _
import django_tables2 as tables

from .models import Registration


class RegistrationTable(tables.Table):
    """
    Table of all registrations.
    """
    last_name = tables.LinkColumn(
        'registration:user_detail',
        args=[tables.A("user_id")],
        verbose_name=lambda: _("last name").capitalize(),
        accessor="user__last_name",
    )

    class Meta:
        attrs = {
            'class': 'table table condensed table-striped',
        }
        model = Registration
        fields = ('last_name', 'user__first_name', 'user__email', 'type',)
        template_name = 'django_tables2/bootstrap4.html'
