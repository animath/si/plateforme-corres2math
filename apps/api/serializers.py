# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.contrib.auth.models import User
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    """
    Serialize a User object into JSON.
    """
    class Meta:
        model = User
        exclude = (
            'username',
            'password',
            'groups',
            'user_permissions',
        )
