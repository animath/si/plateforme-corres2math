# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from unittest.case import skipIf

from django.conf import settings
from django.contrib.auth.models import User
from django.test import TestCase


class TestAPIPages(TestCase):
    def setUp(self):
        self.user = User.objects.create_superuser(
            username="admin",
            password="apitest",
            email="",
        )
        self.client.force_login(self.user)

    def test_user_page(self):
        response = self.client.get("/api/user/")
        self.assertEqual(response.status_code, 200)

    @skipIf("logs" not in settings.INSTALLED_APPS, reason="logs app is not used")
    def test_logs_page(self):
        response = self.client.get("/api/logs/")
        self.assertEqual(response.status_code, 200)
