# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.conf import settings
from django.conf.urls import include, url
from rest_framework import routers

from .viewsets import UserViewSet

# Routers provide an easy way of automatically determining the URL conf.
# Register each app API router and user viewset
router = routers.DefaultRouter()
router.register('user', UserViewSet)

if "logs" in settings.INSTALLED_APPS:
    from logs.api.urls import register_logs_urls
    register_logs_urls(router, "logs")

app_name = 'api'

# Wire up our API using automatic URL routing.
# Additionally, we include login URLs for the browsable API.
urlpatterns = [
    url('^', include(router.urls)),
    url('^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]
