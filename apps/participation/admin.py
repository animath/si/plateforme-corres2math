# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.contrib import admin
from django.utils.translation import gettext_lazy as _

from .models import Participation, Phase, Question, Team, Video


@admin.register(Team)
class TeamAdmin(admin.ModelAdmin):
    list_display = ('name', 'trigram', 'problem', 'valid',)
    search_fields = ('name', 'trigram',)
    list_filter = ('participation__problem', 'participation__valid',)

    def problem(self, team):
        return team.participation.get_problem_display()

    problem.short_description = _('problem number')

    def valid(self, team):
        return team.participation.valid

    valid.short_description = _('valid')


@admin.register(Participation)
class ParticipationAdmin(admin.ModelAdmin):
    list_display = ('team', 'problem', 'valid',)
    search_fields = ('team__name', 'team__trigram',)
    list_filter = ('problem', 'valid',)


@admin.register(Video)
class VideoAdmin(admin.ModelAdmin):
    list_display = ('participation', 'link',)
    search_fields = ('participation__team__name', 'participation__team__trigram', 'link',)


@admin.register(Question)
class QuestionAdmin(admin.ModelAdmin):
    list_display = ('participation', 'question',)
    search_fields = ('participation__team__name', 'participation__team__trigram', 'question',)


@admin.register(Phase)
class PhaseAdmin(admin.ModelAdmin):
    list_display = ('phase_number', 'start', 'end',)
    ordering = ('phase_number', 'start',)
