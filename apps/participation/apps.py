# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.apps import AppConfig
from django.db.models.signals import post_save, pre_delete, pre_save


class ParticipationConfig(AppConfig):
    """
    The participation app contains the data about the teams, videos, ...
    """
    name = 'participation'

    def ready(self):
        from participation.signals import create_team_participation, delete_related_videos, update_mailing_list
        pre_save.connect(update_mailing_list, "participation.Team")
        pre_delete.connect(delete_related_videos, "participation.Participation")
        post_save.connect(create_team_participation, "participation.Team")
