# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django import template

from ..models import Phase


def current_phase(nb):
    phase = Phase.current_phase()
    return phase is not None and phase.phase_number == nb


register = template.Library()
register.filter("current_phase", current_phase)
