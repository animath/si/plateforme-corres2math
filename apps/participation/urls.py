# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.urls import path
from django.views.generic import TemplateView

from .views import CalendarView, CreateQuestionView, CreateTeamView, DeleteQuestionView, JoinTeamView, \
    MyParticipationDetailView, MyTeamDetailView, ParticipationDetailView, PhaseUpdateView, \
    SetParticipationReceiveParticipationView, SetParticipationSendParticipationView, TeamAuthorizationsView, \
    TeamDetailView, TeamLeaveView, TeamListView, TeamUpdateView, UpdateQuestionView, UploadVideoView


app_name = "participation"

urlpatterns = [
    path("create_team/", CreateTeamView.as_view(), name="create_team"),
    path("join_team/", JoinTeamView.as_view(), name="join_team"),
    path("teams/", TeamListView.as_view(), name="team_list"),
    path("team/", MyTeamDetailView.as_view(), name="my_team_detail"),
    path("team/<int:pk>/", TeamDetailView.as_view(), name="team_detail"),
    path("team/<int:pk>/update/", TeamUpdateView.as_view(), name="update_team"),
    path("team/<int:pk>/authorizations/", TeamAuthorizationsView.as_view(), name="team_authorizations"),
    path("team/leave/", TeamLeaveView.as_view(), name="team_leave"),
    path("detail/", MyParticipationDetailView.as_view(), name="my_participation_detail"),
    path("detail/<int:pk>/", ParticipationDetailView.as_view(), name="participation_detail"),
    path("detail/upload-video/<int:pk>/", UploadVideoView.as_view(), name="upload_video"),
    path("detail/<int:pk>/receive-participation/", SetParticipationReceiveParticipationView.as_view(),
         name="participation_receive_participation"),
    path("detail/<int:pk>/send-participation/", SetParticipationSendParticipationView.as_view(),
         name="participation_send_participation"),
    path("detail/<int:pk>/add-question/", CreateQuestionView.as_view(), name="add_question"),
    path("update-question/<int:pk>/", UpdateQuestionView.as_view(), name="update_question"),
    path("delete-question/<int:pk>/", DeleteQuestionView.as_view(), name="delete_question"),
    path("calendar/", CalendarView.as_view(), name="calendar"),
    path("calendar/<int:pk>/", PhaseUpdateView.as_view(), name="update_phase"),
    path("chat/", TemplateView.as_view(template_name="participation/chat.html"), name="chat")
]
