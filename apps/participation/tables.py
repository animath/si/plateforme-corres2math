# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.utils import timezone
from django.utils.translation import gettext_lazy as _
import django_tables2 as tables

from .models import Phase, Team


class CalendarTable(tables.Table):
    class Meta:
        attrs = {
            'class': 'table table condensed table-striped',
        }
        row_attrs = {
            'class': lambda record: 'bg-success' if timezone.now() > record.end else
                                    'bg-warning' if timezone.now() > record.start else
                                    'bg-danger',
            'data-id': lambda record: str(record.phase_number),
        }
        model = Phase
        fields = ('phase_number', 'description', 'start', 'end',)
        template_name = 'django_tables2/bootstrap4.html'
        order_by = ('phase_number',)


# noinspection PyTypeChecker
class TeamTable(tables.Table):
    name = tables.LinkColumn(
        'participation:team_detail',
        args=[tables.A("id")],
        verbose_name=lambda: _("name").capitalize(),
    )

    problem = tables.Column(
        accessor="participation__problem",
        verbose_name=lambda: _("problem number").capitalize(),
    )

    class Meta:
        attrs = {
            'class': 'table table condensed table-striped',
        }
        row_attrs = {
            'class': lambda record: '' if record.participation.solution.link else 'bg-warning',
        }
        model = Team
        fields = ('name', 'trigram', 'problem',)
        template_name = 'django_tables2/bootstrap4.html'


# noinspection PyTypeChecker
class ParticipationTable(tables.Table):
    name = tables.LinkColumn(
        'participation:participation_detail',
        args=[tables.A("id")],
        verbose_name=lambda: _("name").capitalize(),
        accessor="team__name",
    )

    trigram = tables.Column(
        verbose_name=lambda: _("trigram").capitalize(),
        accessor="team__trigram",
    )

    problem = tables.Column(
        verbose_name=lambda: _("problem number").capitalize(),
    )

    class Meta:
        attrs = {
            'class': 'table table condensed table-striped',
        }
        model = Team
        fields = ('name', 'trigram', 'problem',)
        template_name = 'django_tables2/bootstrap4.html'


class VideoTable(tables.Table):
    participation_name = tables.LinkColumn(
        'participation:participation_detail',
        args=[tables.A("participation__pk")],
        verbose_name=lambda: _("name").capitalize(),
        accessor=tables.A("participation__team__name"),
    )

    class Meta:
        attrs = {
            'class': 'table table condensed table-striped',
        }
        model = Team
        fields = ('participation_name', 'link',)
        template_name = 'django_tables2/bootstrap4.html'
