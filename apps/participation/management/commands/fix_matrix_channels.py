# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

import os

from asgiref.sync import async_to_sync
from corres2math.matrix import Matrix, RoomPreset, RoomVisibility
from django.core.management import BaseCommand
from registration.models import AdminRegistration, Registration


class Command(BaseCommand):
    def handle(self, *args, **options):
        Matrix.set_display_name("Bot des Correspondances")

        if not os.getenv("SYNAPSE_PASSWORD"):
            avatar_uri = "plop"
        else:  # pragma: no cover
            if not os.path.isfile(".matrix_avatar"):
                stat_file = os.stat("corres2math/static/logo.png")
                with open("corres2math/static/logo.png", "rb") as f:
                    resp = Matrix.upload(f, filename="logo.png", content_type="image/png",
                                         filesize=stat_file.st_size)[0][0]
                avatar_uri = resp.content_uri
                with open(".matrix_avatar", "w") as f:
                    f.write(avatar_uri)
                Matrix.set_avatar(avatar_uri)

            with open(".matrix_avatar", "r") as f:
                avatar_uri = f.read().rstrip(" \t\r\n")

        if not async_to_sync(Matrix.resolve_room_alias)("#faq:correspondances-maths.fr"):
            Matrix.create_room(
                visibility=RoomVisibility.public,
                alias="faq",
                name="FAQ",
                topic="Posez toutes vos questions ici !",
                federate=False,
                preset=RoomPreset.public_chat,
            )

        if not async_to_sync(Matrix.resolve_room_alias)("#annonces:correspondances-maths.fr"):
            Matrix.create_room(
                visibility=RoomVisibility.public,
                alias="annonces",
                name="Annonces",
                topic="Informations importantes des Correspondances",
                federate=False,
                preset=RoomPreset.public_chat,
            )

        if not async_to_sync(Matrix.resolve_room_alias)("#je-cherche-une-equipe:correspondances-maths.fr"):
            Matrix.create_room(
                visibility=RoomVisibility.public,
                alias="je-cherche-une-equipe",
                name="Je cherche une équipe",
                topic="Le Tinder des Correspondances",
                federate=False,
                preset=RoomPreset.public_chat,
            )

        if not async_to_sync(Matrix.resolve_room_alias)("#flood:correspondances-maths.fr"):
            Matrix.create_room(
                visibility=RoomVisibility.public,
                alias="flood",
                name="Flood",
                topic="Discutez de tout et de rien !",
                federate=False,
                preset=RoomPreset.public_chat,
            )

        Matrix.set_room_avatar("#annonces:correspondances-maths.fr", avatar_uri)
        Matrix.set_room_avatar("#faq:correspondances-maths.fr", avatar_uri)
        Matrix.set_room_avatar("#je-cherche-une-equipe:correspondances-maths.fr", avatar_uri)
        Matrix.set_room_avatar("#flood:correspondances-maths.fr", avatar_uri)

        Matrix.set_room_power_level_event("#annonces:correspondances-maths.fr", "events_default", 50)

        for r in Registration.objects.all():
            Matrix.invite("#annonces:correspondances-maths.fr", f"@{r.matrix_username}:correspondances-maths.fr")
            Matrix.invite("#faq:correspondances-maths.fr", f"@{r.matrix_username}:correspondances-maths.fr")
            Matrix.invite("#je-cherche-une-equipe:correspondances-maths.fr",
                          f"@{r.matrix_username}:correspondances-maths.fr")
            Matrix.invite("#flood:correspondances-maths.fr", f"@{r.matrix_username}:correspondances-maths.fr")

        for admin in AdminRegistration.objects.all():
            Matrix.set_room_power_level("#annonces:correspondances-maths.fr",
                                        f"@{admin.matrix_username}:correspondances-maths.fr", 95)
            Matrix.set_room_power_level("#faq:correspondances-maths.fr",
                                        f"@{admin.matrix_username}:correspondances-maths.fr", 95)
            Matrix.set_room_power_level("#je-cherche-une-equipe:correspondances-maths.fr",
                                        f"@{admin.matrix_username}:correspondances-maths.fr", 95)
            Matrix.set_room_power_level("#flood:correspondances-maths.fr",
                                        f"@{admin.matrix_username}:correspondances-maths.fr", 95)
