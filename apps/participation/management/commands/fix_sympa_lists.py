# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from corres2math.lists import get_sympa_client
from django.core.management import BaseCommand
from django.db.models import Q
from participation.models import Team
from registration.models import CoachRegistration, StudentRegistration


class Command(BaseCommand):
    def handle(self, *args, **options):
        """
        Create Sympa mailing lists and register teams.
        """
        sympa = get_sympa_client()

        sympa.create_list("equipes", "Équipes des Correspondances", "hotline",
                          "Liste de diffusion pour contacter toutes les équipes validées des Correspondances.",
                          "education", raise_error=False)
        sympa.create_list("equipes-non-valides", "Équipes des Correspondances", "hotline",
                          "Liste de diffusion pour contacter toutes les équipes non-validées des Correspondances.",
                          "education", raise_error=False)

        for problem in range(1, 4):
            sympa.create_list(f"probleme-{problem}",
                              f"Équipes des Correspondances participant au problème {problem}", "hotline",
                              f"Liste de diffusion pour contacter les équipes participant au problème {problem}"
                              f" des Correspondances.", "education", raise_error=False)

        for team in Team.objects.filter(participation__valid=True).all():
            team.create_mailing_list()
            sympa.subscribe(team.email, "equipes", f"Equipe {team.name}", True)
            sympa.subscribe(team.email, f"probleme-{team.participation.problem}", f"Equipe {team.name}", True)

        for team in Team.objects.filter(Q(participation__valid=False) | Q(participation__valid__isnull=True)).all():
            team.create_mailing_list()
            sympa.subscribe(team.email, "equipes-non-valides", f"Equipe {team.name}", True)

        for student in StudentRegistration.objects.filter(team__isnull=False).all():
            sympa.subscribe(student.user.email, f"equipe-{student.team.trigram.lower()}", True, f"{student}")
        for coach in CoachRegistration.objects.filter(team__isnull=False).all():
            sympa.subscribe(coach.user.email, f"equipe-{coach.team.trigram.lower()}", True, f"{coach}")
