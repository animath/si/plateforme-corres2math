# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from corres2math.lists import get_sympa_client
from participation.models import Participation, Team, Video


def create_team_participation(instance, created, **_):
    """
    When a team got created, create an associated team and create Video objects.
    """
    participation = Participation.objects.get_or_create(team=instance)[0]
    if not participation.solution:
        participation.solution = Video.objects.create()
    if not participation.synthesis:
        participation.synthesis = Video.objects.create()
    participation.save()
    if not created:
        participation.team.create_mailing_list()


def update_mailing_list(instance: Team, **_):
    """
    When a team name or trigram got updated, update mailing lists and Matrix rooms
    """
    if instance.pk:
        old_team = Team.objects.get(pk=instance.pk)
        if old_team.name != instance.name or old_team.trigram != instance.trigram:
            # TODO Rename Matrix room
            # Delete old mailing list, create a new one
            old_team.delete_mailing_list()
            instance.create_mailing_list()
            # Subscribe all team members in the mailing list
            for student in instance.students.all():
                get_sympa_client().subscribe(student.user.email, f"equipe-{instance.trigram.lower()}", False,
                                             f"{student.user.first_name} {student.user.last_name}")
            for coach in instance.coachs.all():
                get_sympa_client().subscribe(coach.user.email, f"equipe-{instance.trigram.lower()}", False,
                                             f"{coach.user.first_name} {coach.user.last_name}")


def delete_related_videos(instance: Participation, **_):
    if instance.solution:
        instance.solution.delete()
    if instance.synthesis:
        instance.synthesis.delete()
