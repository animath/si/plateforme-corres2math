# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from haystack import indexes

from .models import Participation, Team, Video


class TeamIndex(indexes.ModelSearchIndex, indexes.Indexable):
    """
    Index all teams by their name and trigram.
    """
    text = indexes.NgramField(document=True, use_template=True)

    class Meta:
        model = Team


class ParticipationIndex(indexes.ModelSearchIndex, indexes.Indexable):
    """
    Index all participations by their team name and team trigram.
    """
    text = indexes.NgramField(document=True, use_template=True)

    class Meta:
        model = Participation


class VideoIndex(indexes.ModelSearchIndex, indexes.Indexable):
    """
    Index all teams by their team name and team trigram.
    """
    text = indexes.NgramField(document=True, use_template=True)

    class Meta:
        model = Video
