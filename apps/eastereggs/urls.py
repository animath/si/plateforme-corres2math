# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.urls import path
from django.views.generic import TemplateView

app_name = "eastereggs"

urlpatterns = [
    path("xp/", TemplateView.as_view(template_name="eastereggs/xp.html")),
]
