# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.apps import AppConfig


class EastereggsConfig(AppConfig):
    name = 'eastereggs'
