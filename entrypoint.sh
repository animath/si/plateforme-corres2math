#!/bin/sh

crond -l 0

python manage.py migrate
python manage.py loaddata initial

nginx

if [ "$CORRES2MATH_STAGE" = "prod" ]; then
    gunicorn -b 0.0.0.0:8000 --workers=2 --threads=4 --worker-class=gthread corres2math.wsgi --access-logfile '-' --error-logfile '-';
else
    python manage.py runserver 0.0.0.0:8000;
fi
