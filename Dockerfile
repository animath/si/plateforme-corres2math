FROM python:3.8-alpine

ENV PYTHONUNBUFFERED 1
ENV DJANGO_ALLOW_ASYNC_UNSAFE 1

# Install LaTeX requirements
RUN apk add --no-cache gettext nginx gcc libc-dev libffi-dev libxml2-dev libxslt-dev postgresql-dev libmagic

RUN apk add --no-cache bash

RUN mkdir /code
WORKDIR /code
COPY requirements.txt /code/requirements.txt
RUN pip install -r requirements.txt --no-cache-dir

COPY . /code/

RUN python manage.py collectstatic --noinput && \
        python manage.py compilemessages

# Configure nginx
RUN mkdir /run/nginx
RUN ln -sf /dev/stdout /var/log/nginx/access.log && ln -sf /dev/stderr /var/log/nginx/error.log
RUN ln -sf /code/nginx_corres2math.conf /etc/nginx/conf.d/corres2math.conf
RUN rm /etc/nginx/conf.d/default.conf

RUN crontab /code/corres2math.cron

# With a bashrc, the shell is better
RUN ln -s /code/.bashrc /root/.bashrc

ENTRYPOINT ["/code/entrypoint.sh"]
EXPOSE 80

CMD ["./manage.py", "shell_plus", "--ptpython"]
