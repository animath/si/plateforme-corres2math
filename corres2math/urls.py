# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

"""corres2math URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.contrib import admin
from django.urls import include, path
from django.views.defaults import bad_request, page_not_found, permission_denied, server_error
from django.views.generic import TemplateView
from registration.views import PhotoAuthorizationView

from .views import AdminSearchView

urlpatterns = [
    path('', TemplateView.as_view(template_name="index.html"), name='index'),
    path('about/', TemplateView.as_view(template_name="about.html"), name='about'),
    path('i18n/', include('django.conf.urls.i18n')),
    path('admin/doc/', include('django.contrib.admindocs.urls')),
    path('admin/', admin.site.urls, name="admin"),
    path('accounts/', include('django.contrib.auth.urls')),
    path('search/', AdminSearchView.as_view(), name="haystack_search"),

    path('api/', include('api.urls')),
    path('participation/', include('participation.urls')),
    path('registration/', include('registration.urls')),

    path('media/authorization/photo/<str:filename>/', PhotoAuthorizationView.as_view(), name='photo_authorization'),

    path('', include('eastereggs.urls')),
]

if 'cas_server' in settings.INSTALLED_APPS:  # pragma: no cover
    urlpatterns += [
        path('cas/', include('cas_server.urls', namespace="cas_server")),
    ]

handler400 = bad_request
handler403 = permission_denied
handler404 = page_not_found
handler500 = server_error
