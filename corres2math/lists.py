# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

import os

_client = None


def get_sympa_client():
    global _client
    if _client is None:
        if os.getenv("SYMPA_PASSWORD", None) is not None:  # pragma: no cover
            from sympasoap import Client
            _client = Client("https://" + os.getenv("SYMPA_URL"))
            _client.login(os.getenv("SYMPA_EMAIL"), os.getenv("SYMPA_PASSWORD"))
        else:
            _client = FakeSympaSoapClient()
    return _client


class FakeSympaSoapClient:
    """
    Simulate a Sympa Soap client to run tests, if no Sympa instance is connected.
    """
    def __getattribute__(self, item):
        return lambda *args, **kwargs: None
