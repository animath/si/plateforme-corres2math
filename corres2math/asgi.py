# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

"""
ASGI config for corres2math project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/3.0/howto/deployment/asgi/
"""

import os

from django.core.asgi import get_asgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'corres2math.settings')

application = get_asgi_application()
