# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

import os

from django.core.handlers.asgi import ASGIHandler
from django.core.handlers.wsgi import WSGIHandler
from django.test import TestCase


class TestLoadModules(TestCase):
    """
    Load modules that are not used in development mode in order to increase coverage.
    """
    def test_asgi(self):
        from corres2math import asgi
        self.assertTrue(isinstance(asgi.application, ASGIHandler))

    def test_wsgi(self):
        from corres2math import wsgi
        self.assertTrue(isinstance(wsgi.application, WSGIHandler))

    def test_load_production_settings(self):
        os.putenv("CORRES2MATH_STAGE", "prod")
        os.putenv("DJANGO_DB_TYPE", "postgres")
        from corres2math import settings_prod
        self.assertFalse(settings_prod.DEBUG)
