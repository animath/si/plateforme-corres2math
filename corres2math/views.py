# Copyright (C) 2020 by Animath
# SPDX-License-Identifier: GPL-3.0-or-later

from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.exceptions import PermissionDenied
from haystack.generic_views import SearchView


class AdminMixin(LoginRequiredMixin):
    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated:
            return self.handle_no_permission()
        if not request.user.registration.is_admin:
            raise PermissionDenied
        return super().dispatch(request, *args, **kwargs)


class AdminSearchView(AdminMixin, SearchView):
    pass
